<?php

namespace Drupal\simple_amp\EventSubscriber;

use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Drupal\simple_amp\Render\AmpAttachmentsResponseProcessorInterface;
use Drupal\simple_amp\Render\AmpResponse;
use Drupal\simple_amp\AmpBase;

/**
 * Response subscriber to handle AMP responses.
 */
class AmpResponseSubscriber implements EventSubscriberInterface {

  /**
   * The AMP response attachments processor service.
   *
   * @var \Drupal\simple_amp\Render\AmpAttachmentsResponseProcessorInterface
   */
  protected $ampResponseAttachmentsProcessor;

  /**
   * Constructs a AmpResponseSubscriber object.
   *
   * @param \Drupal\simple_amp\Render\AmpAttachmentsResponseProcessorInterface $html_response_amp_attachments_processor
   *   The AMP response attachments processor service.
   */
  public function __construct(AmpAttachmentsResponseProcessorInterface $amp_response_attachments_processor) {
    $this->ampResponseAttachmentsProcessor = $amp_response_attachments_processor;
  }

  /**
   * Alters the wrapper format if this is an AMP request.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent $event
   *   The event to process.
   */
  public function onAmpRoute(GetResponseEvent $event) {
    $request = $event->getRequest();
    $path = $request->getRequestUri();

    if (substr($path, -4) != '/amp') {
      return;
    }
    else {
      $path = substr($path, 0, -4);
    }
    $entity = \Drupal::service('simple_amp.entity_finder')->findEntity($path);

    if (!empty($entity)) {
      $this->amp = (new AmpBase())
        ->setEntity($entity);
      if ($this->amp->isAmpEnabled()) {
        $request->request->set(MainContentViewSubscriber::WRAPPER_FORMAT, 'simple_amp');
      }
    }
  }

  /**
   * Processes AMP attachments for HtmlResponse responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\FilterResponseEvent $event
   *   The event to process.
   */
  public function onRespond(FilterResponseEvent $event) {
    $request = $event->getRequest();

    $format = $request->request->get(MainContentViewSubscriber::WRAPPER_FORMAT);

    if ($format != 'simple_amp') {
      return;
    }
    $response = $event->getResponse();

    if (!$response instanceof AmpResponse) {
      return;
    }
    $event->setResponse($this->ampResponseAttachmentsProcessor->processAmpAttachments($response));
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];

    // Run before main_content_view_subscriber.
    $events[KernelEvents::REQUEST][] = ['onAmpRoute', 30];

    return $events;
  }

}
