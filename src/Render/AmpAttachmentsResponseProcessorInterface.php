<?php

namespace Drupal\simple_amp\Render;

/**
 * Defines an interface for processing attachments of responses that have them.
 *
 * @see \Drupal\simple_amp\Render\AmpResponse
 * @see \Drupal\simple_amp\Render\AmpResponseAttachmentsProcessor
 */
interface AmpAttachmentsResponseProcessorInterface {

  /**
   * Processes the attachments of a response that has attachments.
   *
   * HTML <head> tags are attached to render arrays using
   * the #amp_attached property. The #amp_attached property is an associative array,
   * where the keys are the attachment types and the values are the attached
   * data.
   *
   * The available keys are:
   * - 'amp_html_head' (tags in HTML <head>)
   *
   * @param \Drupal\simple_amp\Render\AmpAttachmentsInterface $response
   *   The response to process.
   *
   * @return \Drupal\simple_amp\Render\AmpAttachmentsInterface
   *   The processed response, with the attachments updated to reflect their
   *   final values.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the $response parameter is not the type of response object
   *   the processor expects.
   */
  public function processAmpAttachments(AmpAttachmentsInterface $response);

}
