<?php

namespace Drupal\simple_amp\Render;

use Drupal\Core\Render\Markup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\EnforcedResponseException;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Component\Utility\Html;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Processes attachments of HTML responses.
 *
 * This class is used by the rendering service to process the #attached part of
 * the render array, for HTML responses.
 *
 * To render attachments to HTML for testing without a controller, use the
 * 'bare_html_page_renderer' service to generate a
 * Drupal\Core\Render\HtmlResponse object. Then use its getContent(),
 * getStatusCode(), and/or the headers property to access the result.
 *
 * @see template_preprocess_html()
 * @see \Drupal\Core\Render\AttachmentsResponseProcessorInterface
 * @see \Drupal\Core\Render\BareHtmlPageRenderer
 * @see \Drupal\Core\Render\HtmlResponse
 * @see \Drupal\Core\Render\MainContent\HtmlRenderer
 */
class AmpResponseAttachmentsProcessor implements AmpAttachmentsResponseProcessorInterface {

  /**
   * A config object for the system performance configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The renderer.
   *
   * @var \Drupal\simple_amp\Render\BaseAmpRenderer
   */
  protected $renderer;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a HtmlResponseAttachmentsProcessor object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\simple_amp\Render\BaseAmpRenderer $renderer
   *   The renderer.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack, BaseAmpRenderer $renderer, ModuleHandlerInterface $module_handler) {
    $this->config = $config_factory->get('system.performance');
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function processAmpAttachments(AmpAttachmentsInterface $response) {
    // @todo Convert to assertion once https://www.drupal.org/node/2408013 lands

    if (!$response instanceof AmpResponse) {
      throw new \InvalidArgumentException('\Drupal\simple_amp\Render\AmpResponse instance expected.');
    }

    // First, render the actual placeholders; this may cause additional
    // attachments to be added to the response, which the attachment
    // placeholders rendered by renderHtmlResponseAttachmentPlaceholders() will
    // need to include.
    //
    // @todo Exceptions should not be used for code flow control. However, the
    //   Form API does not integrate with the HTTP Kernel based architecture of
    //   Drupal 8. In order to resolve this issue properly it is necessary to
    //   completely separate form submission from rendering.
    //   @see https://www.drupal.org/node/2367555
    try {
      $response = $this->renderPlaceholders($response);
    }
    catch (EnforcedResponseException $e) {
      return $e->getResponse();
    }

    // Get a reference to the attachments.
    $attached = $response->getAttachments();

    // Send a message back if the render array has unsupported #attached types.
    $unsupported_types = array_diff(
      array_keys($attached),
      ['amp_html_head', 'html_response_amp_attachment_placeholders', 'placeholders']
    );
    if (!empty($unsupported_types)) {
      throw new \LogicException(sprintf('You are not allowed to use %s in #amp_attached.', implode(', ', $unsupported_types)));
    }
    // If we don't have any placeholders, there is no need to proceed.
    if (!empty($attached['html_response_amp_attachment_placeholders'])) {

      // Get the placeholders from attached and then remove them.
      $attachment_placeholders = $attached['html_response_amp_attachment_placeholders'];
      unset($attached['html_response_amp_attachment_placeholders']);

      // Since we can only replace content in the HTML head section if there's a
      // placeholder for it, we can safely avoid processing the render array if
      // it's not present.

      if (!empty($attachment_placeholders['amphead'])) {
        // Process 'amp_html_head'
        if (!empty($attached['amp_html_head'])) {
          $variables['amphead'] = $this->processAmpHtmlHead($attached['amp_html_head']);
        }
      }

      // Now replace the attachment placeholders.
      if ($variables != NULL) {
        $this->renderAmpResponseAttachmentPlaceholders($response, $attachment_placeholders, $variables);
      }
      
    }
    // AttachmentsResponseProcessorInterface mandates that the response it
    // processes contains the final attachment values.
    $response->setAttachments($attached);

    return $response;
  }

  /**
   * Renders placeholders (#amp_attached['placeholders']).
   *
   * First, the HTML response object is converted to an equivalent render array,
   * with #markup being set to the response's content and #attached being set to
   * the response's attachments. Among these attachments, there may be
   * placeholders that need to be rendered (replaced).
   *
   * Next, RendererInterface::renderRoot() is called, which renders the
   * placeholders into their final markup.
   *
   * The markup that results from RendererInterface::renderRoot() is now the
   * original HTML response's content, but with the placeholders rendered. We
   * overwrite the existing content in the original HTML response object with
   * this markup. The markup that was rendered for the placeholders may also
   * have attachments (e.g. for CSS/JS assets) itself, and cacheability metadata
   * that indicates what that markup depends on. That metadata is also added to
   * the HTML response object.
   *
   * @param \Drupal\Core\Render\HtmlResponse $response
   *   The HTML response whose placeholders are being replaced.
   *
   * @return \Drupal\Core\Render\HtmlResponse
   *   The updated HTML response, with replaced placeholders.
   *
   * @see \Drupal\Core\Render\Renderer::replacePlaceholders()
   * @see \Drupal\Core\Render\Renderer::renderPlaceholder()
   */
  protected function renderPlaceholders(AmpResponse $response) {
    $build = [
      '#markup' => Markup::create($response->getContent()),
      '#amp_attached' => $response->getAttachments(),
    ];
    // RendererInterface::renderRoot() renders the $build render array and
    // updates it in place. We don't care about the return value (which is just
    // $build['#markup']), but about the resulting render array.
    // @todo Simplify this when https://www.drupal.org/node/2495001 lands.
    $this->renderer->renderRoot($build);

    // Update the Response object now that the placeholders have been rendered.

    $placeholders_bubbleable_metadata = AmpBubbleableMetadata::createFromRenderArray($build);
    $response
      ->setContent($build['#markup'])
      ->setAttachments($response->getAttachments());
    return $response;
  }

  /**
   * Renders HTML response attachment placeholders.
   *
   * This is the last step where all of the attachments are placed into the
   * response object's contents.
   *
   * @param \Drupal\Core\Render\HtmlResponse $response
   *   The HTML response to update.
   * @param array $placeholders
   *   An array of placeholders, keyed by type with the placeholders
   *   present in the content of the response as values.
   * @param array $variables
   *   The variables to render and replace, keyed by type with renderable
   *   arrays as values.
   */
  protected function renderAmpResponseAttachmentPlaceholders(AmpResponse $response, array $placeholders, array $variables) {
    $content = $response->getContent();
    foreach ($placeholders as $type => $placeholder) {
      if (isset($variables[$type])) {
        $content = str_replace($placeholder, $this->renderer->renderPlain($variables[$type]), $content);
      }
    }
    $response->setContent($content);
  }

  /**
   * Ensure proper key/data order and defaults for renderable head items.
   *
   * @param array $amp_html_head
   *   The ['#amp_attached']['amp_html_head'] portion of a render array.
   *
   * @return array
   *   The ['#amp_attached']['amp_html_head'] portion of a render array with #type of
   *   amp_html_tag added for items without a #type.
   */
  protected function processAmpHtmlHead(array $amp_html_head) {
    $head = [];
    $schema_metatags = [];
    foreach ($amp_html_head as $item) {
      list($data, $key) = $item;
      if (array_key_exists('schema_metatag', $data['#attributes'])) {
        // Schema metatag: First time through, collect and nest by group.
        $group = $data['#attributes']['group'];
        // Nest items by the group they are in.
        $name = $data['#attributes']['name'];
        $content = $data['#attributes']['content'];
        $schema_metatags[$group][$name] = $content;
        unset($amp_html_head[$key]);

        continue;
      }

      if (array_key_exists('rel', $data['#attributes']) && $data['#attributes']['rel'] == 'canonical') {
        continue;
      }

      if (!isset($data['#type'])) {
        $data['#type'] = 'html_tag';
      }
      $head[$key] = $data;
    }

    // Schema metatag: Second time through, replace group name with index,
    // and add JSON LD wrappers.
    $items = [];
    $group_key = 0;
    foreach ($schema_metatags as $group_name => $data) {
      if (empty($items)) {
        $items['@context'] = 'https://schema.org';
      }
      $items['@graph'][$group_key] = $data;
      $group_key++;
    }
    // Schema metatag: Turn the structured data array into JSON LD
    // and add it to page head.
    if (count($items) > 0) {
      $jsonld = json_encode($items, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_UNESCAPED_UNICODE);
      if (!empty($jsonld)) {
        $head['schema_metatag'] = [
          [
            '#type' => 'html_tag',
            '#tag' => 'script',
            '#value' => $jsonld,
            '#attributes' => ['type' => 'application/ld+json'],
          ],
        ];
      }
    }
    return $head;
  }

}
