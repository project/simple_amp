<?php

namespace Drupal\simple_amp\Render;

/**
 * Defines an interface for responses that can expose #amp_attached metadata.
 *
 * @see \Drupal\simple_amp\Render\AmpAttachmentsTrait
 */
interface AmpAttachmentsInterface {

  /**
   * Gets AMP attachments.
   *
   * @return array
   *   The AMP attachments.
   */
  public function getAttachments();

  /**
   * Adds AMP attachments.
   *
   * @param array $attachments
   *   The AMP attachments to add.
   *
   * @return $this
   */
  public function addAttachments(array $attachments);

  /**
   * Sets AMP attachments.
   *
   * @param array $attachments
   *   The AMP attachments to set.
   *
   * @return $this
   */
  public function setAttachments(array $attachments);

}
