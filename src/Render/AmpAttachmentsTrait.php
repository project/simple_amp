<?php

namespace Drupal\simple_amp\Render;

use Drupal\Core\Render\AttachmentsTrait;

/**
 * Provides an implementation of AttachmentsInterface.
 *
 * @see \Drupal\simple_amp\Render\AmpAttachmentsInterface
 */
trait AmpAttachmentsTrait {

  use AttachmentsTrait {
    AttachmentsTrait::getAttachments as parentGetAttachments;
    AttachmentsTrait::setAttachments as parentSetAttachments;
  }

  /**
   * {@inheritdoc}
   */
  public function addAttachments(array $attachments) {
    $this->attachments = AmpBubbleableMetadata::mergeAttachments($this->attachments, $attachments);
    return $this;
  }

}
