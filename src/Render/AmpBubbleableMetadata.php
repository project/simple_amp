<?php

namespace Drupal\simple_amp\Render;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Value object used for bubbleable rendering metadata.
 */
class AmpBubbleableMetadata extends BubbleableMetadata {

  use AmpAttachmentsTrait;

  /**
   * {@inheritdoc}
   */
  public function merge(CacheableMetadata $other) {
    $result = parent::merge($other);

    // This is called many times per request, so avoid merging unless absolutely
    // necessary.
    if ($other instanceof AmpBubbleableMetadata) {
      if (empty($this->attachments)) {
        $result->attachments = $other->attachments;
      }
      elseif (empty($other->attachments)) {
        $result->attachments = $this->attachments;
      }
      else {
        $result->attachments = static::mergeAttachments($this->attachments, $other->attachments);
      }
    }
    
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function applyTo(array &$build) {
    parent::applyTo($build);
    $build['#amp_attached'] = $this->attachments;
  }


  /**
   * {@inheritdoc}
   */
  public static function createFromRenderArray(array $build) {
    $meta = parent::createFromRenderArray($build);

    $meta->attachments = (isset($build['#amp_attached'])) ? $build['#amp_attached'] : [];
    return $meta;
  }

}
