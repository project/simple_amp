<?php

namespace Drupal\simple_amp\Render;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseTrait;
use Symfony\Component\HttpFoundation\Response;

/**
 * A response that contains and can expose cacheability metadata and attachments.
 *
 * @see \Drupal\Core\Cache\CacheableResponse
 * @see \Drupal\Core\Render\AttachmentsInterface
 * @see \Drupal\simple_amp\Render\AmpAttachmentsTrait
 */
class AmpResponse extends Response implements AmpAttachmentsInterface {

  use AmpAttachmentsTrait;
  use CacheableResponseTrait;

  /**
   * {@inheritdoc}
   */
  public function setContent($content) {
    // A render array can automatically be converted to a string and set the
    // necessary metadata.
    if (is_array($content) && (isset($content['#markup']))) {
      $content += [
        '#amp_attached' => [
          'html_response_amp_attachment_placeholders' => [],
          'placeholders' => [],
        ],
      ];
      $this->addCacheableDependency(CacheableMetadata::createFromRenderArray($content));
      $this->setAttachments($content['#amp_attached']);
      $content = $content['#markup'];
    }

    return parent::setContent($content);
  }

}
