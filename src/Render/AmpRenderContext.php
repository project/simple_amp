<?php

namespace Drupal\simple_amp\Render;

use Drupal\Core\Render\RenderContext;

/**
 * The render context: a stack containing bubbleable rendering metadata.
 *
 * A stack of \Drupal\simple_amp\Render\AmpBubbleableMetadata objects.
 *
 * @see \Drupal\Core\Render\RendererInterface
 * @see \Drupal\Core\Render\Renderer
 * @see \Drupal\Core\Render\BubbleableMetadata
 * @see \Drupal\simple_amp\Render\AmpBubbleableMetadata
 */
class AmpRenderContext extends RenderContext {

  /**
   * {@inheritdoc}
   */
  public function update(&$element) {
    // The latest frame represents the bubbleable metadata for the subtree.
    $frame = $this->pop();
    // Update the frame, but also update the current element, to ensure it
    // contains up-to-date information in case it gets render cached.
    $updated_frame = AmpBubbleableMetadata::createFromRenderArray($element)->merge($frame);
    $updated_frame->applyTo($element);
    $this->push($updated_frame);
  }

}
