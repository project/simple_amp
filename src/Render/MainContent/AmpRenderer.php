<?php

namespace Drupal\simple_amp\Render\MainContent;

use Drupal\simple_amp\Render\AmpResponse;
use Drupal\simple_amp\Render\BaseAmpRenderer;
use Drupal\simple_amp\Render\BaseAmpRenderCache;
use Drupal\simple_amp\Render\AmpRenderContext;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Display\PageVariantInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Display\ContextAwareVariantInterface;
use Drupal\Core\Render\PageDisplayVariantSelectionEvent;
use Drupal\Core\Render\RenderEvents;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Render\MainContent\HtmlRenderer;
use Drupal\Core\Render\MainContent\MainContentRendererInterface;

/**
 * Default main content renderer for AMP requests.
 *
 * For attachment handling of AMP responses:
 * @see Drupal\simple_amp\Controller\AmpController::assembleAmpPage()
 * @see \Drupal\simple_amp\Render\AmpAttachmentsResponseProcessorInterface
 * @see \Drupal\simple_amp\Render\AmpResponse
 * @see \Drupal\simple_amp\Render\AmpResponseAttachmentsProcessor
 */
class AmpRenderer extends HtmlRenderer implements MainContentRendererInterface {

  /**
   * Constructs a new HtmlRenderer.
   *
   * @param \Drupal\Core\Controller\TitleResolverInterface $title_resolver
   *   The title resolver.
   * @param \Drupal\Component\Plugin\PluginManagerInterface $display_variant_manager
   *   The display variant manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\simple_amp\Render\BaseAmpRenderer $renderer
   *   The renderer service.
   * @param \Drupal\simple_amp\Render\BaseAmpRenderCache $render_cache
   *   The render cache service.
   * @param array $renderer_config
   *   The renderer configuration array.
   */
  public function __construct(TitleResolverInterface $title_resolver, PluginManagerInterface $display_variant_manager, EventDispatcherInterface $event_dispatcher, ModuleHandlerInterface $module_handler, BaseAmpRenderer $renderer, BaseAmpRenderCache $render_cache, array $renderer_config) {
    $this->titleResolver = $title_resolver;
    $this->displayVariantManager = $display_variant_manager;
    $this->eventDispatcher = $event_dispatcher;
    $this->moduleHandler = $module_handler;
    $this->renderer = $renderer;
    $this->renderCache = $render_cache;
    $this->rendererConfig = $renderer_config;
  }

  /**
   * {@inheritdoc}
   *
   * The entire HTML: takes a #type 'amp' and wraps it in a #type 'html'.
   */
  public function renderResponse(array $main_content, Request $request, RouteMatchInterface $route_match) {
    list($page, $title) = $this->prepare($main_content, $request, $route_match);
    if (!isset($page['#type']) || $page['#type'] !== 'amp_page') {
      throw new \LogicException('Must be #type amp_page');
    }

    $page['#title'] = $title;
    // Now render the rendered page.html.twig template inside the html.html.twig
    // template, and use the bubbled #attached metadata from $page to ensure we
    // load all attached assets.
    $html = [
      '#type' => 'amp',
      'page' => $page,
    ];

    // Render, but don't replace placeholders yet, because that happens later in
    // the render pipeline. To not replace placeholders yet, we use
    // RendererInterface::render() instead of RendererInterface::renderRoot().
    // @see \Drupal\simple_amp\Render\AmpResponseAttachmentsProcessor.
    $render_context = new AmpRenderContext();
    $this->renderer->executeInRenderContext($render_context, function () use (&$html) {
      // RendererInterface::render() renders the $html render array and updates
      // it in place. We don't care about the return value (which is just
      // $html['#markup']), but about the resulting render array.
      // @todo Simplify this when https://www.drupal.org/node/2495001 lands.
      $this->renderer->render($html);
    });
    // RendererInterface::render() always causes bubbleable metadata to be
    // stored in the render context, no need to check it conditionally.
    $bubbleable_metadata = $render_context->pop();
    $bubbleable_metadata->applyTo($html);
    $content = $this->renderCache->getCacheableRenderArray($html);

    // Also associate the required cache contexts.
    // (Because we use ::render() above and not ::renderRoot(), we manually must
    // ensure the HTML response varies by the required cache contexts.)
    $content['#cache']['contexts'] = Cache::mergeContexts($content['#cache']['contexts'], $this->rendererConfig['required_cache_contexts']);

    // Also associate the "rendered" cache tag. This allows us to invalidate the
    // entire render cache, regardless of the cache bin.
    $content['#cache']['tags'][] = 'rendered';
    $response = new AmpResponse($content, 200, [
      'Content-Type' => 'text/html; charset=UTF-8',
    ]);

    return $response;
  }

  /**
   * Prepares the HTML body: wraps the main content in #type 'page'.
   *
   * @param array $main_content
   *   The render array representing the main content.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object, for context.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match, for context.
   *
   * @return array
   *   An array with two values:
   *   0. A #type 'page' render array.
   *   1. The page title.
   *
   * @throws \LogicException
   *   If the selected display variant does not implement PageVariantInterface.
   */
  protected function prepare(array $main_content, Request $request, RouteMatchInterface $route_match) {
    // Determine the title: use the title provided by the main content if any,
    // otherwise get it from the routing information.
    $get_title = function (array $main_content) use ($request, $route_match) {
      return isset($main_content['#title']) ? $main_content['#title'] : $this->titleResolver->getTitle($request, $route_match->getRouteObject());
    };

    // If the _controller result already is #type => amp_page,
    // we have no work to do: The "main content" already is an entire "amp_page"
    // (see amp.html.twig).
    if (isset($main_content['#type']) && $main_content['#type'] === 'amp_page') {
      $page = $main_content;
      $title = $get_title($page);
    }
    // Allow hooks to add attachments to $page['#amp_attached'].
    $this->invokePageAmpAttachmentHooks($page);

    return [$page, $title];
  }

  /**
   * Invokes the page amp_attachment hooks.
   *
   * @param array &$page
   *   A #type 'page' render array, for which the AMP page attachment hooks will be
   *   invoked and to which the results will be added.
   *
   * @throws \LogicException
   *
   * @internal
   *
   * @see hook_amp_page_attachments()
   * @see hook_amp_page_attachments_alter()
   */
  public function invokePageAmpAttachmentHooks(array &$page) {
    // Modules can add AMP attachments.
    $attachments = [];
    foreach ($this->moduleHandler->getImplementations('simple_amp_page_attachments') as $module) {
      $function = $module . '_simple_amp_page_attachments';
      $function($attachments);
    }
    if (array_diff(array_keys($attachments), ['#amp_attached']) !== []) {
      throw new \LogicException('Only #amp_attached may be set in hook_simple_amp_page_attachments().');
    }

    // Modules and themes can alter AMP page attachments.
    $this->moduleHandler->alter('amp_page_attachments', $attachments);
    \Drupal::theme()->alter('amp_page_attachments', $attachments);
    if (array_diff(array_keys($attachments), ['#amp_attached']) !== []) {
      throw new \LogicException('Only #amp_attached may be set in hook_simple_amp_page_attachments_alter().');
    }

    // Merge the AMP attachments onto the $page render array.
    $page = $this->renderer->mergeBubbleableMetadata($page, $attachments);
  }

}
