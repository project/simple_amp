<?php

namespace Drupal\simple_amp\Render;

use Drupal\Core\Render\PlaceholderingRenderCache;

/**
 * Wraps the caching logic for the render caching system.
 */
class BaseAmpRenderCache extends PlaceholderingRenderCache {

  /**
   * {@inheritdoc}
   */
  public function getCacheableRenderArray(array $elements) {
    $data = parent::getCacheableRenderArray($elements);
    $data['#amp_attached'] = $elements['page']['#amp_attached'];
    return $data;
  }

}
