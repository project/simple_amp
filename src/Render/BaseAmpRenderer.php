<?php

namespace Drupal\simple_amp\Render;

use Drupal\Core\Render\Renderer;
use Drupal\Core\Render\RendererInterface;

/**
 * Turns a render array into a HTML string.
 */
class BaseAmpRenderer extends Renderer {

  /**
   * {@inheritdoc}
   */
  public function renderPlaceholder($placeholder, array $elements) {
    // Get the render array for the given placeholder
    $placeholder_elements = $elements['#amp_attached']['placeholders'][$placeholder];

    // Prevent the render array from being auto-placeholdered again.
    $placeholder_elements['#create_placeholder'] = FALSE;

    // Render the placeholder into markup.
    $markup = $this->renderPlain($placeholder_elements);

    // Replace the placeholder with its rendered markup, and merge its
    // bubbleable metadata with the main elements'.
    $elements['#markup'] = Markup::create(str_replace($placeholder, $markup, $elements['#markup']));
    $elements = $this->mergeBubbleableMetadata($elements, $placeholder_elements);

    // Remove the placeholder that we've just rendered.
    unset($elements['#amp_attached']['placeholders'][$placeholder]);

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  protected function replacePlaceholders(array &$elements) {
    if (!isset($elements['#amp_attached']['placeholders']) || empty($elements['#amp_attached']['placeholders'])) {
      return FALSE;
    }

    foreach ($elements['#amp_attached']['placeholders'] as $placeholder => $placeholder_element) {
      $elements = $this->renderPlaceholder($placeholder, $elements);
    }

    return TRUE;
  }

  /**
   * See the docs for ::render().
   */
  protected function doRender(&$elements, $is_root_call = FALSE) {
    parent::doRender($elements, $is_root_call);
    unset($elements['#attached']);
    $elements['#amp_attached'] = isset($elements['#amp_attached']) ? $elements['#amp_attached'] : [];
    return $elements['#markup'];
  }

  /**
   * {@inheritdoc}
   */
  public function mergeBubbleableMetadata(array $a, array $b) {
    $meta_a = AmpBubbleableMetadata::createFromRenderArray($a);
    $meta_b = AmpBubbleableMetadata::createFromRenderArray($b);
    $meta_a->merge($meta_b)->applyTo($a);
    return $a;
  }

}
