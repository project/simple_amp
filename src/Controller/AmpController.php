<?php

namespace Drupal\simple_amp\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\simple_amp\AmpBase;
use Drupal\simple_amp\Render\AmpResponse;
use Drupal\Component\Utility\Crypt;

class AMPController extends ControllerBase {

  protected $amp;
  protected $renderer;

  public function __construct(Renderer $render) {
    $this->renderer = $render;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * AMP version of the node.
   */
  public function page(Request $request, $entity = NULL) {
    $access = FALSE;
    if ($entity instanceof EntityInterface) {
      $access = $entity->access('view');
    }
    if ($access && is_object($entity)) {
      $this->amp = (new AmpBase())
        ->setEntity($entity)
        ->parse();
      if ($this->amp->isAmpEnabled()) {
        return \Drupal::service('main_content_renderer.simple_amp')->renderResponse($this->assembleAmpPage(), $request, \Drupal::routeMatch());
      }
    }
    $url = Url::fromRoute('entity.node.canonical', ['node' => $entity->id()]);
    return new RedirectResponse($url->toString(), 301);
  }

  /**
   * Build renderable page array.
   */
  protected function assembleAmpPage() {
    $entity = $this->amp->getEntity();

    $variables['placeholder_token'] = Crypt::randomBytesBase64(55);
    $placeholder = '<link rel="amp-head-placeholder" content="token=' . $variables['placeholder_token'] . '">';
    return [
      '#theme'     => 'amp',
      '#type'      => 'amp_page',
      '#amp_attached' => [
        'html_response_amp_attachment_placeholders' => [
          'amphead' => $placeholder
        ],
        'placeholders' => []
      ],
      '#title'     => $entity->getTitle(),
      '#entity'    => $entity,
      '#content'   => $this->amp->getContent(),
      '#params'    => [
        'metadata'  => $this->amp->getMetadata(),
        'attached_placeholder_token' => $placeholder,
        'scripts'   => $this->amp->getScripts(),
        'canonical' => $this->amp->getCanonicalUrl(),
        'ga'        => $this->amp->getGoogleAnalytics(),
      ],
      '#cache' => [
        'contexts' => [
          'url',
          'url.site',
          'url.query_args',
        ],
        'tags' => [
          'node:' . $entity->id(),
        ],
      ],
    ];
  }

}
