<?php

namespace Drupal\simple_amp;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

class AmpEntityFinder {

  /**
   * Derive entity data from a given path.
   *
   * @param $path
   *   The drupal path, e.g. /node/2.
   *
   * @return $entity|NULL
   */
  public function findEntity($path) {
    $entity = NULL;
    $route = \Drupal::routeMatch()->getRouteObject();

    if(!isset($route)) {
      return $entity;
    }
    $route_path = $route->getPath();
    $amp_route = FALSE;
    // /amp is not a valid route path.
    if(substr($route_path, -4) == '/amp') {
      $route_path = substr($route_path, 0 ,-4);
      $route_path = str_replace('{entity}', '{node}', $route_path);
      $amp_route = TRUE;
    }

    $type = $this->findEntityType($path);
    $links = $this->findEntityLinks($path);

    if($links == NULL) {
      return $entity;
    }

    // Check that the route pattern is an entity template.
    if (in_array($route_path, $links) || $amp_route) {
      $parts = explode('/', $route_path);
      $i = 0;
      foreach ($parts as $part) {
        if (!empty($part)) {
          $i++;
        }
        if ($part == '{' . $type . '}') {
          break;
        }
      }
      $i++;
      // Get entity path if alias.
      $path_without_langcode = urldecode(substr($path, 6));

      $entity_path = \Drupal::service('path_alias.manager')->getPathByAlias($path_without_langcode);

      // Look! We're using arg() in Drupal 8 because we have to.
      $args = explode('/', $entity_path);

      if (isset($args[$i])) {
        $entity = \Drupal::entityTypeManager()->getStorage($type)->load($args[$i]);
      }
      if(isset($args[$i - 1]) && $args[$i - 1] != 'node') {
        $entity = \Drupal::entityTypeManager()->getStorage($type)->load($args[$i - 1]);
      }

    }

    return $entity;
  }

  /**
   * Get entity links, given an entity type
   *
   * @param $type
   *   The drupal path, e.g. 'taxonomy_term'.
   *
   * @return $entity_links|NULL
   */
  public function getLinksByType($type) {
    $entity_manager = \Drupal::entityTypeManager();
    $entity_type = $entity_manager->getDefinition($type);
    return $entity_type->getLinkTemplates();
  }

  /**
   * Derive entity type links from a given path.
   *
   * @param $path
   *   The drupal path, e.g. /node/2.
   *
   * @return $entity_links|NULL
   */
  public function findEntityLinks($path) {
    $entity_links = NULL;
    $route = \Drupal::routeMatch()->getRouteObject();
    if(!isset($route)) {
      return $entity_links;
    }
    $route_path = $route->getPath();
    $amp_route = FALSE;
    // /amp is not a valid route path.
    if(substr($route_path, -4) == '/amp') {
      $route_path = substr($route_path, 0 ,-4);
      $amp_route = TRUE;
    }
    $links_node = $this->getLinksByType('node');
    $links_taxonomy = $this->getLinksByType('taxonomy_term');
    if (in_array($route_path, $links_node) || $amp_route) {
      $entity_links = $links_node;
    }
    if (in_array($route_path, $links_taxonomy)) {
      $entity_links = $links_taxonomy;
    }
    return $entity_links;
  }

  /**
   * Derive entity type from a given path.
   *
   * @param $path
   *   The drupal path, e.g. /node/2.
   *
   * @return $entity_type|NULL
   */
  public function findEntityType($path) {
    $entity_type = NULL;
    $route = \Drupal::routeMatch()->getRouteObject();
    if(!isset($route)) {
      return $entity_type;
    }
    $route_path = $route->getPath();
    $amp_route = FALSE;
    // /amp is not a valid route path.
    if(substr($route_path, -4) == '/amp') {
      $route_path = substr($route_path, 0 ,-4);
      $amp_route = TRUE;
    }
    $links_node = $this->getLinksByType('node');
    $links_taxonomy = $this->getLinksByType('taxonomy_term');
    if (in_array($route_path, $links_node) || $amp_route) {
      $entity_type = 'node';
    }
    if (in_array($route_path, $links_taxonomy)) {
      $entity_type = 'taxonomy_term';
    }
    return $entity_type;
  }

}