<?php

namespace Drupal\simple_amp\Plugin\AmpComponent;

use Drupal\simple_amp\AmpComponentBase;

/**
 * Form AMP component.
 *
 * @AmpComponent(
 *   id = "amp-form",
 *   name = @Translation("Form"),
 *   description = @Translation("Enables JS to display forms"),
 *   regexp = {}
 * )
 */
class Form extends AmpComponentBase {

  /**
   * {@inheritdoc}
   */
  public function getElement() {
    return '<script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>';
  }

}
