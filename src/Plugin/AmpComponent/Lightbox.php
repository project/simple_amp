<?php

namespace Drupal\simple_amp\Plugin\AmpComponent;

use Drupal\simple_amp\AmpComponentBase;

/**
 * Lightbox AMP component.
 *
 * @AmpComponent(
 *   id = "amp-lightbox",
 *   name = @Translation("Lightbox"),
 *   description = @Translation("Enables JS for a lightbox (modal)"),
 *   regexp = {}
 * )
 */
class Lightbox extends AmpComponentBase {

  /**
   * {@inheritdoc}
   */
  public function getElement() {
    return '<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>';
  }

}
