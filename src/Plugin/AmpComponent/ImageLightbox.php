<?php

namespace Drupal\simple_amp\Plugin\AmpComponent;

use Drupal\simple_amp\AmpComponentBase;

/**
 * Image Lightbox AMP component.
 *
 * @AmpComponent(
 *   id = "amp-image-lightbox",
 *   name = @Translation("Image Lightbox"),
 *   description = @Translation("Enables JS for an image lightbox (modal)"),
 *   regexp = {}
 * )
 */
class ImageLightbox extends AmpComponentBase {

  /**
   * {@inheritdoc}
   */
  public function getElement() {
    return '<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>';
  }

}
